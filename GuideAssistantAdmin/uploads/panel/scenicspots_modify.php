<?php
require ('../include/init.inc.php');
$scenicspots_code = $township_code = $scenicspots_name
= $scenicspots_summary = $scenicspots_ticketprice 
= $scenicspots_longitude = $scenicspots_latitude 
= $scenicspots_isactivity = $scenicspots_voiceurl
= $scenicspots_cover = '';
extract ( $_REQUEST, EXTR_IF_EXISTS );

Common::checkParam($scenicspots_code);

$scenicspots = ScenicSpots::getScenicSpotsById( $scenicspots_code );
if(empty($scenicspots)){
	Common::exitWithError("此景点不存在" ,"panel/scenicspots.php");
}

if (Common::isPost ()) {
	
	if($scenicspots_name =="" || $township_code == "" 
	    || $scenicspots_ticketprice == "" || $scenicspots_longitude == ""
	    || $scenicspots_latitude == ""){
			OSAdmin::alert("error",ErrorMessage::NEED_PARAM);
	}else{
		$exist = false;
		$scenicspots_exist = ScenicSpots::getScenicSpotsByName($scenicspots_name);
		if(!empty($scenicspots_exist)){
			if($scenicspots_code!=$scenicspots_exist['ScenicSpotsCode']){
				$exist=true;
				OSAdmin::alert("error",'此名称已存在');
			}
		}
		if(!$exist){
		    $update_data = array ('ScenicSpotsName' => $scenicspots_name,
		        'ScenicSpotsSummary' => $scenicspots_summary ,'ScenicSpotsLongitude' =>$scenicspots_longitude,
		        'ScenicSpotsLatitude' =>$scenicspots_latitude, 'TicketPrice' => $scenicspots_ticketprice,
		         'TownshipCode' => $township_code);
		    
		    $current_time = time();
		    $upload_result = true;
		    if ($_FILES['scenicspots_voiceurl']['error'] == 0) {
		        $scenicspots_voiceurl = UploadFile::upload('scenicspots_voiceurl', '../up/',
		            array('audio/mp3', 'audio/wmv'), 1024 * 2, 'VOC' . $current_time);
		        if (is_numeric($scenicspots_voiceurl)) { 
		            $msg = UploadFile::getErrorMessage($scenicspots_voiceurl);
		            OSAdmin::alert("error", '语音上传出错,原因:' . $msg);
		            $upload_result = false;
		        } else {
		            $update_data['VoiceURL'] = $scenicspots_voiceurl;
		        }
		    }
		    if ($_FILES['scenicspots_cover']['error'] == 0) {
		        $scenicspots_cover = UploadFile::upload('scenicspots_cover', '../up/',
		            array('image/jpeg', 'image/jpg', 'image/bmp', 'image/png'), 1024, 'IMG' . $current_time);
		        
		        if (is_numeric($scenicspots_cover)) {
		            $msg = UploadFile::getErrorMessage($scenicspots_cover);
		            OSAdmin::alert("error", '封面上传出错,原因:' . $msg);
		            $upload_result = false;
		        } else {
		            $update_data['ScenicSpotsCover'] = $scenicspots_cover;
		        }
		    }
		    
			if ($upload_result) {
			    $result = ScenicSpots::updateScenicSpots( $scenicspots_code,$update_data );
			    	
			    if ($result>=0) {
			        SysLog::addLog ( UserSession::getUserName(), 'MODIFY', 'ScenicSpots' ,$scenicspots_code, json_encode($update_data) );
			        Common::exitWithSuccess ('更新完成','panel/scenicspots.php');
			    } else {
			        OSAdmin::alert("error");
			    }    
			}
			
		}
	}
}

$township_options_list = TownShipInfo::getTownShipForOptions();

Template::assign ( 'scenicspots', $scenicspots );
Template::assign ( 'township_options_list', $township_options_list );
Template::display ( 'panel/scenicspots_modify.tpl' );