<?php
require ('../include/init.inc.php');
$VersionCode = $VersionName = $VersionTitle = $VersionBrief = $DownloadUrl ='';
$IsLastVersion = 1;
extract ( $_POST, EXTR_IF_EXISTS );

if (Common::isPost ()) {
    $exist = APPVersions::getVersionByName($VersionName);
    if($exist){
        OSAdmin::alert("error",ErrorMessage::NAME_CONFLICT);
    } else if ($VersionCode == "" || $VersionName == "" || DownloadUrl == "") {
        OSAdmin::alert("error",ErrorMessage::NEED_PARAM);
    } else {
        $input_data = array ('VersionCode' => $VersionCode, 'VersionName' => $VersionName, 'VersionTitle' => $VersionTitle, 'VersionBrief' => $VersionBrief, 'DownloadUrl' => $DownloadUrl, 'IsLastVersion' => $IsLastVersion);
        $updateID = APPVersions::updateIsLastVersion();
        if($updateID) {
            //插入数据
            $ID = APPVersions::addVersionInfo($input_data);
            if($ID) {
                SysLog::addLog ( UserSession::getUserName(), 'ADD', 'APPVersion' , $ID, json_encode($input_data) );
    			Common::exitWithSuccess ('版本信息添加成功','panel/versions.php');
            }
        }
    }
}
Template::assign("_POST" ,$_POST);
Template::display('panel/app_version_add.tpl' );