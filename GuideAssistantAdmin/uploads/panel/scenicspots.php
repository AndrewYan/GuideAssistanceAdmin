<?php
require ('../include/init.inc.php');
$method = $page_no = $scenicspots_code = '';
extract ( $_REQUEST, EXTR_IF_EXISTS );

if ($method == 'pause' && ! empty ( $scenicspots_code )) {
    $scenicspots_data=array("IsActivity"=>0);

    $result = ScenicSpots::updateScenicSpots( $scenicspots_code,$scenicspots_data );
    if ($result>=0) {
        SysLog::addLog ( UserSession::getUserName(), 'PAUSE',  'ScenicSpots' ,$scenicspots_code ,json_encode($scenicspots_data) );
        Common::exitWithSuccess ( '已停用','panel/scenicspots.php' );
    }else{
        OSAdmin::alert("error");
    }
}

if ($method == 'play' && ! empty ( $scenicspots_code )) {
    $scenicspots_data=array("IsActivity"=>1);
    $result = ScenicSpots::updateScenicSpots ( $scenicspots_code,$scenicspots_data );
    if ($result>=0) {
        SysLog::addLog ( UserSession::getUserName(), 'PLAY' , 'ScenicSpots' ,$scenicspots_code ,json_encode($scenicspots_data) );
        Common::exitWithSuccess ( '已启用','panel/scenicspots.php' );
    }else{
        OSAdmin::alert("error");
    }
}

//START 数据库查询及分页数据
$row_count = ScenicSpots::count ();
$page_size = PAGE_SIZE;
$page_no=$page_no<1?1:$page_no;
$total_page=$row_count%$page_size==0?$row_count/$page_size:ceil($row_count/$page_size);
$total_page=$total_page<1?1:$total_page;
$page_no=$page_no>($total_page)?($total_page):$page_no;
$start = ($page_no - 1) * $page_size;

$current_user_info=UserSession::getSessionInfo();
$user_group = $current_user_info['user_group'];
$current_user_id = $current_user_info['user_id'];

$scenicspots = ScenicSpots::getScenicSpots($start, $page_size);

//追加操作的确认层
$confirm_html = OSAdmin::renderJsConfirm("icon-pause,icon-play");

$page_html=Pagination::showPager("",$page_no,PAGE_SIZE,$row_count);

Template::assign ( 'page_no', $page_no );
Template::assign ( 'page_size', PAGE_SIZE );
Template::assign ( 'row_count', $row_count );
Template::assign ( 'page_html', $page_html );
Template::assign ( 'scenicspots', $scenicspots );
Template::assign ( 'osadmin_action_confirm' , $confirm_html);
Template::assign ( 'user_group', $user_group );
Template::assign ( 'current_user_id', $current_user_id );
Template::display ( 'panel/scenicspots.tpl' );