<?php
require ('../include/init.inc.php');
$ID = $VersionCode = $VersionName = $VersionTitle = $VersionBrief = $DownloadUrl = '';
extract ( $_REQUEST, EXTR_IF_EXISTS );

Common::checkParam($ID);


$version = APPVersions::getVersionInfoByID($ID);

if(empty($version)){
    Common::exitWithError(ErrorMessage::MODULE_NOT_EXIST,"panel/versions.php");
}

if (Common::isPost ()) {
    if($VersionCode == "" || $VersionName == "" || DownloadUrl == "" ){
        OSAdmin::alert("error",ErrorMessage::NEED_PARAM);
    } else {
        $update_data = array ('VersionCode' => $VersionCode, 'VersionName' => $VersionName, 'VersionTitle' => $VersionTitle ,'VersionBrief' => $VersionBrief ,
            'DownloadUrl' =>$DownloadUrl);
        print_r($update_data);
        
        $result = APPVersions::updateInfo($update_data, $ID);
        if ($result>=0) {
            SysLog::addLog ( UserSession::getUserName(), 'MODIFY', 'Version' ,$ID, json_encode($update_data) );
            Common::exitWithSuccess ('更新完成','panel/versions.php');
        } else {
            OSAdmin::alert("error");
        }
    }
}

Template::assign ( 'version', $version );
//Template::assign ( 'module_online_optioins', $module_online_optioins );
Template::display ( 'panel/app_version_modify.tpl' );