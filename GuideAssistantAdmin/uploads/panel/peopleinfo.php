<?php
require ('../include/init.inc.php');
$method = $page_no = '';
extract ( $_GET, EXTR_IF_EXISTS );

//START 数据库查询及分页数据
$row_count = PeopleInfo::count ();
$page_size = PAGE_SIZE;
$page_no=$page_no<1?1:$page_no;
$total_page=$row_count%$page_size==0?$row_count/$page_size:ceil($row_count/$page_size);
$total_page=$total_page<1?1:$total_page;
$page_no=$page_no>($total_page)?($total_page):$page_no;
$start = ($page_no - 1) * $page_size;

$peopleinfo = PeopleInfo::getPeopleInfo($start, $page_size);

$confirm_html = OSAdmin::renderJsConfirm("icon-remove");

$page_html=Pagination::showPager("",$page_no,PAGE_SIZE,$row_count);

Template::assign ( 'page_no', $page_no );
Template::assign ( 'page_size', PAGE_SIZE );
Template::assign ( 'row_count', $row_count );
Template::assign ( 'page_html', $page_html );
Template::assign ( 'peopleinfo', $peopleinfo );
Template::assign ( 'osadmin_action_confirm' , $confirm_html);
Template::display ( 'panel/peopleinfo.tpl' );
