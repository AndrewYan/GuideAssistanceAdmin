<?php
require ('../include/init.inc.php');
$page_no = $start_date = $end_date ="";
extract ( $_GET, EXTR_IF_EXISTS );

$start_time = strtotime($start_date);
$end_time = strtotime($end_date);
//START 数据库查询及分页数据
if($start_date != '' && $end_date !=''){
    $row_count =UserPhoneInfo::getCountByDate($start_date,$end_date);
}else{
    $row_count = UserPhoneInfo::count ();
}

$page_size = PAGE_SIZE;
$page_no=$page_no<1?1:$page_no;

$total_page=$row_count%$page_size==0?$row_count/$page_size:ceil($row_count/$page_size);
$total_page=$total_page<1?1:$total_page;
$page_no=$page_no>($total_page)?($total_page):$page_no;

$start = ($page_no - 1) * $page_size;
//END
$UserPhoneInfo = UserPhoneInfo::getAllUserPhoneInfo($start, $page_size, $start_time,$end_time);

$page_html=Pagination::showPager("user_phone_info.php?start_date=$start_date&end_date=$end_date",$page_no,PAGE_SIZE,$row_count);

Template::assign ( 'page_no', $page_no );
Template::assign ( 'page_size', PAGE_SIZE );
Template::assign ( 'row_count', $row_count );
Template::assign ( 'page_html', $page_html );
Template::assign ( '_GET', $_GET );
Template::assign ( 'UserPhoneInfo', $UserPhoneInfo );
Template::display ( 'panel/app_user_phone_info.tpl' );
