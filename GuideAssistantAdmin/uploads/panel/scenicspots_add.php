<?php
require ('../include/init.inc.php');
$scenicspots_code = $township_code = $scenicspots_name
= $scenicspots_summary = $scenicspots_ticketprice 
= $scenicspots_longitude = $scenicspots_latitude 
= $scenicspots_isactivity = '';
$_POST['scenicspots_isactivity'] = 1;
extract ( $_POST, EXTR_IF_EXISTS );


if (Common::isPost ()) {
	$exist = ScenicSpots::getScenicSpotsByName($scenicspots_name);
	if($exist){
		OSAdmin::alert("error",ErrorMessage::NAME_CONFLICT);
	}else if($scenicspots_name =="" || $township_code == "" 
	    || $scenicspots_ticketprice == "" || $scenicspots_longitude == ""
	    || $scenicspots_latitude == ""){
		OSAdmin::alert("error",ErrorMessage::NEED_PARAM);
	}else{
	    
	    $current_time = time();
	    
	    $upload_result = true;
	    $scenicspots_voiceurl = '';
	    $scenicspots_cover = '';
	    if ($_FILES['scenicspots_voiceurl']['error'] == 0) {
	        $scenicspots_voiceurl = UploadFile::upload('scenicspots_voiceurl', '../up/',
	             array('audio/mp3', 'audio/wmv'), 1024 * 2, 'VOC' . $current_time);
	        if (is_numeric($scenicspots_voiceurl)) {
	            $msg = UploadFile::getErrorMessage($scenicspots_voiceurl);
	            OSAdmin::alert("error", '语音上传出错,原因:' . $msg);
	            $upload_result = false;
	        }
	    }
	    if ($_FILES['scenicspots_cover']['error'] == 0) {
	        $scenicspots_cover = UploadFile::upload('scenicspots_cover', '../up/',
	             array('image/jpeg', 'image/jpg', 'image/bmp', 'image/png'), 1024, 'IMG' . $current_time);
	        if (is_numeric($scenicspots_cover)) {
	            $msg = UploadFile::getErrorMessage($scenicspots_cover);
	            OSAdmin::alert("error", '封面上传出错,原因:' . $msg);
	            $upload_result = false;
	        }
	    }
	    
	    if ($upload_result) {
	        $scenicspots_code = 'Spots' . $current_time;
	         
	        $input_data = array ('ScenicSpotsCode' => $scenicspots_code, 'ScenicSpotsName' => $scenicspots_name,
	            'ScenicSpotsSummary' => $scenicspots_summary ,'ScenicSpotsLongitude' =>$scenicspots_longitude,
	            'ScenicSpotsLatitude' =>$scenicspots_latitude, 'VoiceURL' => $scenicspots_voiceurl,
	            'TicketPrice' => $scenicspots_ticketprice, 'TownshipCode' => $township_code,
	            'ScenicSpotsCover' => $scenicspots_cover,  'IsActivity' => $scenicspots_isactivity);
	        ScenicSpots::addScenicSpots( $input_data );
	        
	        if ($scenicspots_code) {
	            SysLog::addLog ( UserSession::getUserName(), 'ADD', 'ScenicSpots' , $scenicspots_code, json_encode($input_data) );
	            Common::exitWithSuccess ('景点添加成功','panel/scenicspots.php');
	        }
	    }

	}
}

$township_list = TownShipInfo::getTownShipForOptions();

Template::assign("township_options_list", $township_list);
Template::assign("_POST" ,$_POST);
Template::display('panel/scenicspots_add.tpl' );
