<?php
require ('../include/init.inc.php');
$StrategyCode = $method = $StrategyTitle = $StrategyCover = $StrategyContent = $GoodNum = $OrderNum = $IsActivity = $page_no = $search = '';
extract ( $_REQUEST, EXTR_IF_EXISTS );

if ($method == 'pause' && ! empty ( $ActivityCode )) {
    $update_data=array("IsActivity"=>0);
    $result = Activity::updateActivityStatue ( $ActivityCode,$update_data );
    if ($result>=0) {
        SysLog::addLog ( UserSession::getUserName(), 'PAUSE',  'Activity' ,$ActivityCode ,json_encode($update_data) );
        Common::exitWithSuccess ( '已封停','panel/activities.php' );
    }else{
        OSAdmin::alert("error");
    }
}

if ($method == 'play' && ! empty ( $StrategyCode )) {
    $update_data=array("IsActivity"=>1);
    $result = Activity::updateActivityStatue ( $ActivityCode,$update_data );
    if ($result>=0) {
        SysLog::addLog ( UserSession::getUserName(), 'PLAY' , 'Activity' ,$ActivityCode ,json_encode($update_data) );
        Common::exitWithSuccess ( '已解封','panel/activities.php' );
    }else{
        OSAdmin::alert("error");
    }
}


//START 数据库查询及分页数据
$page_size = PAGE_SIZE;
$page_no=$page_no<1?1:$page_no;

$row_count = Activity::count ();
print_r($row_count);
$total_page=$row_count%$page_size==0?$row_count/$page_size:ceil($row_count/$page_size);
$total_page=$total_page<1?1:$total_page;
$page_no=$page_no>($total_page)?($total_page):$page_no;
$start = ($page_no - 1) * $page_size;
$Activities = Activity::getAllActivities ( $start , $page_size );


$page_html=Pagination::showPager("acitivities.php",$page_no,$page_size,$row_count);

//追加操作的确认层
$confirm_html = OSAdmin::renderJsConfirm("icon-pause,icon-play,icon-remove");

// 设置模板变量
/* $group_options=UserGroup::getGroupForOptions();
$group_options[0] = "全部";
ksort($group_options); */

/* Template::assign ( 'group_options', $group_options ); */
Template::assign ( 'activities', $Activities );
Template::assign ( '_GET', $_GET );
Template::assign ( 'page_no', $page_no );
Template::assign ( 'page_html', $page_html );
Template::assign ( 'osadmin_action_confirm' , $confirm_html);
Template::display ( 'panel/activities.tpl' );