<?php

class TownShipInfo extends Base {
    // 表名
    private static $table_name = 'townshipinfo';
    // 查询字段
    private static $columns = array('TownshipCode', 'TownshipName', 'TownshipBrief',
        'TownshipCover', 'TownshipIsActivity', 'TownshipOrder');
    //状态定义
    
    public static function getTableName(){
        return self::$table_name;
    }
    
    //列表
    public static function getTownShipInfo() {
        $db=self::__instance();
        $columns = implode(self::$columns,',');
        $sql="select ".$columns." from ".self::getTableName();
        $list = $db->query($sql)->fetchAll();
        if ($list) {
            return $list;
        }
        return array ();
    }
    
    public static function getTownShipForOptions() {
        $township_options_array = array ();
        $township_list = self::getTownShipInfo ();
    
        foreach ( $township_list as $township ) {
            $township_options_array [$township ['TownshipCode']] = $township ['TownshipName'];
        }
    
        return $township_options_array;
    }
}

?>