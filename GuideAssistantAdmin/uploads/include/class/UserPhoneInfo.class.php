<?php
if(!defined('ACCESS')) {exit('Access denied.');}
/**
 * 客户端信息
 * @author yanqi
 *
 */
class UserPhoneInfo extends Base
{
    // 表名
    private static $table_name = 'userphoneinfo';
    // 查询字段
    private static $columns = array('ID', 'PhoneName', 'PhoneNumber', 'LocalIP', 'IMEI','IMSI', 'LocalMAC', 'PSN', 'LoginTime');
    
    /**
     * 获得所有用户手机信息
     */
    public static function getAllUserPhoneInfo($start, $page_size, $start_date='', $end_date='') {
        $db=self::__instance();
        $condition = array();
        $sub_condition = array();
        if($start_date !='' && $end_date !=''){
            $sub_condition["LoginTime[<>]"] =array($start_date,$end_date);
            print_r(array($start_date,$end_date));
        }
        if(empty($sub_condition)){
            $condition = array();
        }else{
            $condition["AND"] = $sub_condition;
        }
        $condition['LIMIT']=array($start,$page_size);
        print_r($db->select ( self::$table_name, self::$columns, $condition ));
        $list = $db->select ( self::$table_name, self::$columns, $condition );
        print_r($list);
        if ($list) {
            return $list;
        }
        return array ();
    }
    
    /**
     * 根据开始结束时间获得数量
     * @param unknown $start_date
     * @param unknown $end_date
     * @return unknown
     */
    public static function getCountByDate($start_date,$end_date) {
        $db=self::__instance();
        $condition=array();
        
        $sub_condition["LoginTime[<>]"] =array($start_date,$end_date);
        $condition["AND"] = $sub_condition;
    
        $num = $db->count ( self::$table_name,$condition );
        return $num;
    }
    
    public static function count() {
        $db=self::__instance();
    
        // $sub_condition = array();
        
        $condition = array();
        
        $num = $db->count ( self::$table_name,$condition);
        return $num;
    }
}

?>