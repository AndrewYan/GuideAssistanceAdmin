<?php
if(!defined('ACCESS')) {exit('Access denied.');}
/**
 * 游赏攻略
 * @author yanqi
 *
 */
class TouringGuide extends Base
{
    // 表名
    private static $table_name = 'strategyinfo';
    // 查询字段
    private static $columns = array('StrategyCode', 'StrategyTitle', 'StrategyCover', 'StrategyContent', 'GoodNum','OrderNum', 'IsActivity');
    
    /**
     * 获得所有游赏攻略
     */
    public static function getAllTouringGuide($start ='' ,$page_size='') {
        $db=self::__instance();
		$limit ="";
		if($page_size){
			$limit =" limit $start,$page_size ";
		}
		$columns = implode(self::$columns,',');
		$sql="select ".$columns." from ".self::$table_name. $limit;
		print_r($sql);
		$list = $db->query($sql)->fetchAll();
		if ($list) {
			return $list;
		}
		return array ();
    }
    
    /**
     * 更新状态
     * @param unknown $StrategyCode 攻略系统编码
     * @param unknown $updateData 更新的内容
     */
    public static function updateTouringGuideStatue($StrategyCode,$updateData) {
        if (! $updateData || ! is_array ( $updateData )) {
            return false;
        }
        $db=self::__instance();
        $condition=array("StrategyCode"=>$StrategyCode);
        
        $id = $db->update ( self::$table_name, $updateData, $condition );
        return $id;
    }
    
    /**
     * 查询记录条数
     * @param string $condition
     * @return unknown
     */
    public static function count($condition = '') {
        $db=self::__instance();
        $num = $db->count ( self::$table_name, $condition );
        return $num;
    }
}

?>