<?php
if(!defined('ACCESS')) {exit('Access denied.');}

class ScenicSpots extends Base {
    // 表名
    private static $table_name = 'scenicspots';
    // 查询字段
    private static $columns = array('ScenicSpotsCode', 'ScenicSpotsName', 'ScenicSpotsSummary',
         'ScenicSpotsLongitude', 'ScenicSpotsLatitude', 'VoiceURL', 'TicketPrice', 'TownshipCode',
        'ScenicSpotsCover', 'IsActivity');
    //状态定义
    
    public static function getTableName(){
        return self::$table_name;
    }

    //列表
    public static function getScenicSpots($start ='' ,$page_size='') {
        $db=self::__instance();
        $limit ="";
        if($page_size){
            $limit =" limit $start,$page_size ";
        }
        $columns = implode(self::$columns,',');
        $sql="select ".$columns." from ".self::getTableName()." ". $limit;
        $list = $db->query($sql)->fetchAll();
        if ($list) {
            return $list;
        }
        return array ();
    }
    
    public static function count($condition = '') {
        $db=self::__instance();
        $num = $db->count ( self::getTableName(), $condition );
        return $num;
    }
    
    public static function getScenicSpotsByName($scenicspots_name) {
        if (! $scenicspots_name || ! is_numeric ( $scenicspots_name )) {
            return false;
        }
        $db=self::__instance();
        $condition['ScenicSpotsName'] = $scenicspots_name;
        $list = $db->select ( self::getTableName(), self::$columns, $condition );
        if ($list) {
            return $list [0];
        }
        return array ();
    }
    
    public static function addScenicSpots($scenicspots_data) {
        if (! $scenicspots_data || ! is_array ( $scenicspots_data )) {
            return false;
        }
        $db=self::__instance();
        $id = $db->insert ( self::getTableName(), $scenicspots_data );
        return $id;
    }
    
    public static function updateScenicSpots($scenicspots_code,$scenicspots_data) {
    
        if (! $scenicspots_data || ! is_array ( $scenicspots_data )) {
            return false;
        }
        $db=self::__instance();
        $condition=array("ScenicSpotsCode"=>$scenicspots_code);
    
        $id = $db->update ( self::getTableName(), $scenicspots_data, $condition );
        return $id;
    }
    
    public static function getScenicSpotsById($scenicspots_code)
    {
        if (!$scenicspots_code) return false;
        $db = self::__instance();
        $condition = array("ScenicSpotsCode" => $scenicspots_code);
        $list = $db->select(self::getTableName(), self::$columns, $condition);
        if ($list) return $list[0];
    
        return array();
    }
    
    
}
