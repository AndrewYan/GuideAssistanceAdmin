<?php
if(!defined('ACCESS')) {exit('Access denied.');}

class PeopleInfo extends Base {
    
    // 表名
    private static $table_name = 'peopleinfo';
    // 查询字段
    private static $columns = array('PeopleSystemCode', 'NAME', 'Sex', 'Age',
         'Address', 'PhoneNumber', 'Birthday', 'SecondPhoneNum',
         'QQ', 'WeChat', 'Weibo', 'Email', 'IsVip', 'InnermostWords', 'NickName');
    //状态定义
    
    public static function getTableName(){
        return self::$table_name;
    }
    
    //列表
    public static function getPeopleInfo($start ='' ,$page_size='') {
        $db=self::__instance();
        $limit ="";
        if($page_size){
            $limit =" limit $start,$page_size ";
        }
        $columns = implode(self::$columns,',');
        $sql="select ".$columns." from ".self::getTableName()." ". $limit;
        $list = $db->query($sql)->fetchAll();
        if ($list) {
            return $list;
        }
        return array ();
    }
    
    public static function count($condition = '') {
        $db=self::__instance();
        $num = $db->count ( self::getTableName(), $condition );
        return $num;
    }
    
    
    
}
