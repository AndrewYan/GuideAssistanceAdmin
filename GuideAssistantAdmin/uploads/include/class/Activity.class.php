<?php
if(!defined('ACCESS')) {exit('Access denied.');}

/**
 * 游赏活动类
 * @author yanqi
 *
 */
class Activity extends Base
{
    // 表名
    private static $table_name = 'activityinfo';
    // 查询字段
    private static $columns = array('ActivityCode', 'ActivityTitle', 'TheBackground', 'ActivityLocation', 'ActivityTime','ActivityEndTime', 'ActivityContent', 'PhoneNum', 'IsActivity', 'ActivityCover','PublishTime','OrderNum');
    
    /**
     * 获得所有活动
     * @param string $start
     * @param string $page_size
     */
    public static function getAllActivities($start ='' ,$page_size='') {
        $db=self::__instance();
        $limit ="";
        if($page_size){
            $limit =" limit $start,$page_size ";
        }
        $columns = implode(self::$columns,',');
        $sql="select ".$columns." from ".self::$table_name. $limit;
        print_r($sql);
        $list = $db->query($sql)->fetchAll();
        if ($list) {
            return $list;
        }
        return array ();
    }
    
    /**
     * 更新状态
     * @param unknown $StrategyCode 活动系统编码
     * @param unknown $updateData 更新的内容
     */
    public static function updateActivityStatue($ActivityCode,$updateData) {
        if (! $updateData || ! is_array ( $updateData )) {
            return false;
        }
        echo($ActivityCode);
        $db=self::__instance();
        $condition=array("ActivityCode"=>$ActivityCode);
        print_r($condition);
        $id = $db->update ( self::$table_name, $updateData, $condition );
        return $id;
    }
    
    /**
     * 查询记录条数
     * @param string $condition
     * @return unknown
     */
    public static function count($condition = '') {
        $db=self::__instance();
        $num = $db->count ( self::$table_name, $condition );
        return $num;
    }
}

?>