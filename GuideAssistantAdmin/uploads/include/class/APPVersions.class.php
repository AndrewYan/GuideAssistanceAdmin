<?php 
if(!defined('ACCESS')) {exit('Access denied.');}
class APPVersions extends Base
{
    // 表名
    private static $table_name = 'versionmanagementinfo';
    // 查询字段
    private static $columns = array('ID', 'VersionCode', 'VersionName', 'VersionTitle', 'VersionBrief','DownloadUrl', 'IsLastVersion');
    
    /**
     * 获得所有版本信息
     * @return unknown|multitype:
     */
    public static function getAllVersionInfo() {
        $db=self::__instance();
        $condition = array();
        
        $list = $db->select ( self::$table_name, self::$columns, $condition );
        print_r($list);
        if ($list) {
            return $list;
        }
        return array ();
    }
    
    /**
     * 根据版本名称获得版本信息
     * @param unknown $VersionName
     */
    public static function getVersionByName($VersionName) {
        if (! $VersionName || ! is_numeric ( $VersionName )) {
            return false;
        }
        $db=self::__instance();
        $condition['VersionName'] = $VersionName;
        $list = $db->select ( self::$table_name, self::$columns, $condition );
        if ($list) {
            return $list [0];
        }
        return array ();
    }
    
    /**
     * 添加版本信息
     * @param unknown $inputData 添加信息
     */
    public static function addVersionInfo($inputData) {
        if (! $inputData || ! is_array ( $inputData )) {
            return false;
        }
        $db=self::__instance();
        $id = $db->insert ( self::$table_name, $inputData );
        return $id;
    }
    
    /**
     * 更新版本信息为旧版
     */
    public static function updateIsLastVersion() {
        $db=self::__instance();
        $condition = array("IsLastVersion" => 1);
        $data = array("IsLastVersion" => 0);
        $ID = $db -> update(self::$table_name, $data, $condition);
        return $ID;
    }
    
    /**
     * 更新信息
     * @param unknown $updateData 更新内容
     * @param unknown $conditionData 更新条件
     */
    public static function updateInfo($updateData, $conditionData) {
        $db=self::__instance();
        if (! $updateData || ! is_array ( $updateData ) || ! $conditionData) {
            return false;
        }
        $condition['ID'] = $conditionData;
        print_r($condition);
        $ID = $db -> update(self::$table_name, $updateData, $condition);
        return $ID;
    }
    
    /**
     * 根据ID获得版本信息
     * @param unknown $VersionID
     */
    public static function getVersionInfoByID($VersionID) {
        $db=self::__instance();
        if (! $VersionID || ! is_numeric ( $VersionID )) {
            return false;
        }
        
        $condition['ID'] = $VersionID;
        
        $list = $db->select ( self::$table_name, self::$columns, $condition );
        if ($list) {
            return $list [0];
        }
        return array ();
    }
}

?>