<?php
if (! defined('ACCESS')) {
    exit('Access denied.');
}

class UploadFile {

    private static $default_size = 1024;
    
    private static $ERROR = 1;
    private static $TYPE_ERROR = 2;
    private static $MAX_ERROR = 3;
    private static $EXSIT_ERROR = 4;

    public static function upload($post_filename, $save_path, $filetype, $max_kbsize, $save_filename) {
        $file_info = $_FILES[$post_filename];
        if (!$post_filename || !$file_info) {
            return false;
        }
        
        if ($file_info["error"] > 0) {
            return self::$ERROR;
        } else {
            if ($filetype) {
                if (is_array($filetype)) { 
                    $type_result = false;
                    foreach ($filetype as $type) {
                        if ($file_info["type"] == $type) {
                            $type_result = true;
                            break;
                        }
                    }
                    if (!$type_result) {
                        return self::$TYPE_ERROR;
                    }
                } else {
                    if (!$file_info["type"] == $filetype) {
                        return self::$TYPE_ERROR;
                    }
                }
            }
            
            if ($max_kbsize) {
                if ($file_info["size"] / 1024 > $max_kbsize) {
                    return self::$MAX_ERROR;
                }
            } else {
                if ($file_info["size"] /  1024 > self::$default_size) {
                    return self::$MAX_ERROR;
                }
            }
            
            if (!$save_filename) {
                $save_filename = $file_info['name'];
            }
            if(!strstr($save_filename, '.')) {
                if (strpos($file_info['name'], '.')) {
                    $save_filename = $save_filename . strstr($file_info['name'], '.');
                }
            }
            
            
            $final_path = $save_path . '/' . $save_filename;
            
            if (!file_exists($save_path)) {
                mkdir($save_path);
            }
            
            if (file_exists($final_path)) {
                return self::$EXSIT_ERROR;
            } else {
                move_uploaded_file($file_info["tmp_name"], $final_path);
            }
            
            return $save_filename;
        }
    }
    
    function ResizeImage($im,$maxwidth,$maxheight,$name){
        //取得当前图片大小
        $width = imagesx($im);
        $height = imagesy($im);
        //生成缩略图的大小
        if(($width > $maxwidth) || ($height > $maxheight)){
            $widthratio = $maxwidth/$width;
            $heightratio = $maxheight/$height;
            if($widthratio < $heightratio){
                $ratio = $widthratio;
            }else{
                $ratio = $heightratio;
            }
            $newwidth = $width * $ratio;
            $newheight = $height * $ratio;
    
            if(function_exists("imagecopyresampled")){
                $newim = imagecreatetruecolor($newwidth, $newheight);
                imagecopyresampled($newim, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
            }else{
                $newim = imagecreate($newwidth, $newheight);
                imagecopyresized($newim, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
            }
            ImageJpeg ($newim,$name . ".jpg");
            ImageDestroy ($newim);
        }else{
            ImageJpeg ($im,$name . ".jpg");
        }
    }
    
    public static function getErrorMessage($error_code) {
        $msg = '';
        switch ($error_code) {
            case self::$ERROR : $msg = '传输出现问题';break;
            case self::$TYPE_ERROR : $msg = '上传格式不符';break;
            case self::$MAX_ERROR : $msg = '容量超出最大限制';break;
            case self::$EXSIT_ERROR : $msg = '文件已经存在';break;
        }
        return $msg;
    }
    
    
    
}

?>