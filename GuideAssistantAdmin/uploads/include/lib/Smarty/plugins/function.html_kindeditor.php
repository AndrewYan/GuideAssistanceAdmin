<?php
/**
 * kindeditor 在线编辑器
 * @author rick <shaoyikai@qq.com>
 *
 * 使用举例：
 * {html_kindeditor name="kindcontent" width="700" height="250" theame="simple" lang="en" items="simple"}
 */
function smarty_function_html_kindeditor($params)
{
    $params['height'] = empty($params['height']) ? 300: $params['height'];
    $params['theame'] = empty($params['theame']) ? 'default': $params['theame'];
    $params['name'] = empty($params['name']) ? 'content': $params['name'];
    $params['lang'] = empty($params['lang']) ? 'zh_CN': $params['lang']; //可选择的语言zh_CN,zh_TW,en,ko,ar
    $params['items'] = empty($params['items']) ? 'default': $params['items'];
    // print_r($params);
    if($params['items'] === 'simple')
    {
        $params['width'] = empty($params['width']) ? 480: $params['width'];
        $items = '["source","preview","code","|","cut","copy","paste","|","justifyleft","justifycenter","justifyright",
    "justifyfull","|","insertorderedlist","insertunorderedlist","indent","outdent","|","subscript",
    "superscript","clearhtml","fullscreen","/",
    "formatblock","fontname","fontsize","|","forecolor","hilitecolor","bold",
    "italic","underline","lineheight","removeformat","|","image","multiimage","table","emoticons","|",
    "anchor","link","unlink"]';
    }
    else
    {
        
        $params['width'] = empty($params['width']) ? 680: $params['width'];
        $items = '["source","|","undo","redo","|","preview","print","template","code","cut","copy","paste","plainpaste","wordpaste","|","justifyleft","justifycenter","justifyright",
    "justifyfull","insertorderedlist","insertunorderedlist","indent","outdent","subscript",
    "superscript","clearhtml","quickformat","selectall","|","fullscreen","/",
    "formatblock","fontname","fontsize","|","forecolor","hilitecolor","bold",
    "italic","underline","strikethrough","lineheight","removeformat","|","image","multiimage",
    "flash","media","insertfile","table","hr","emoticons","baidumap","pagebreak",
    "anchor","link","unlink","|","about"]';
    }
    $editor = '<script charset="utf-8" src="/uploads/include/kindeditor/kindeditor.js"></script>
  <script charset="utf-8" src="/uploads/include/kindeditor/lang/'.$params["lang"].'.js"></script>
  <link rel="stylesheet" href="/uploads/include/kindeditor/themes/'.$params["theame"].'/'.$params["theame"].'.css" />
      
  
   <script>
      alert("aaaa");
    $(function(){
      KindEditor.ready(function(K) {
          window.editor = K.create("#editor_id",{
            
            themeType:"'.$params["theame"].'",
            langType : "'.$params["lang"].'",
            items: '.$items.',
            minWidth: 400,
          });
      })
});
  
</script>
  <textarea id="editor_id" name="'.$params["name"].'" style="width:'.$params["width"].'px;height:'.$params["height"].'px;">
  </textarea>';
    print_r('<script>alert("555555");</script>');
    return $editor;
}
?>