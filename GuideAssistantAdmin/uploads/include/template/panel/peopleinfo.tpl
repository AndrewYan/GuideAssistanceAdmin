<{include file ="header.tpl"}>
<{include file ="navibar.tpl"}>
<{include file ="sidebar.tpl"}>
<!-- TPLSTART 以上内容不需更改，保证该TPL页内的标签匹配即可 -->

<{$osadmin_action_alert}>
<{$osadmin_quick_note}>
<div class="btn-toolbar">
<!-- 	<a href="quicknote_add.php"  class="btn btn-primary"><i class="icon-plus"></i> 用户信息</a> -->
</div>
<div class="block">
	<a href="#page-stats" class="block-heading" data-toggle="collapse">用户信息列表</a>
	<div id="page-stats" class="block-body collapse in">
		<table class="table table-striped">
			<thead>
			<tr>
				<th>用户编码</th>
				<th>姓名</th>
				<th>性别</th>
				<th>年龄</th>
				<th>地址</th>
				<th>手机号码</th>
				<th>出生日期</th>
				<th>副手机号码</th>
				<th>QQ</th>
				<th>微信</th>
				<th>微博</th>
				<th>邮箱</th>
				<th>会员状态</th>
				<th>个性签名</th>
				<th>昵称</th>
			</tr>
			</thead>
			<tbody>							  
			<{foreach name=info from=$peopleinfo item=info}>
				<tr>
				<td><{$info.PeopleSystemCode}></td>
				<td><{$info.NAME}></td>
				<td><{$info.Sex}></td>
				<td><{$info.Age}></td>
				<td><{$info.Address}></td>
				<td><{$info.PhoneNumber}></td>
				<td><{$info.Birthday}></td>
				<td><{$info.SecondPhoneNum}></td>
				<td><{$info.QQ}></td>
				<td><{$info.WeChat}></td>
				<td><{$info.Weibo}></td>
				<td><{$info.Email}></td>
				<td><{$info.IsVip}></td>
				<td><{$info.InnermostWords}></td>
				<td><{$info.NickName}></td>
				<td><{$info.note_content}></td>
				</tr>
			<{/foreach}>
		  </tbody>
		</table>
			<!--- START 分页模板 --->
				
               <{$page_html}>
					
			 <!--- END --->
	</div>
</div>

<!---操作的确认层，相当于javascript:confirm函数--->
<{$osadmin_action_confirm}>
	
<!-- TPLEND 以下内容不需更改，请保证该TPL页内的标签匹配即可 -->
<{include file="footer.tpl" }>