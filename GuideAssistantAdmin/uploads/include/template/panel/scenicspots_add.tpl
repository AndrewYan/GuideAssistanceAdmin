<{include file ="header.tpl"}>
<{include file ="navibar.tpl"}>
<{include file ="sidebar.tpl"}>
<!-- START 以上内容不需更改，保证该TPL页内的标签匹配即可 -->

<{$osadmin_action_alert}>
<{$osadmin_quick_note}>
    
<div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">请填写景点信息</a></li>
    </ul>	
	
	<div id="myTabContent" class="tab-content">
		  <div class="tab-pane active in" id="home">

           <form id="tab" method="post" action="" enctype="multipart/form-data">
				<label>名称</label>
				<input type="text" name="scenicspots_name" class="input-xlarge" required="true" autofocus="true"  value="<{$_POST.scenicspots_name}>">
				<label>所属镇区</label>
				<{html_options name=township_code id="DropDownTimezone" class="input-xlarge" options=$township_options_list selected=0  required="true"}>
				<label>介绍</label>
				<textarea name="scenicspots_summary" rows="3" class="input-xlarge"><{$_POST.scenicspots_summary}></textarea>
				<label>票价</label>
				<input type="number" name="scenicspots_ticketprice" value="<{$_POST.scenicspots_ticketprice}>" class="input-xlarge" required="true" >
				<label>经度 <span class="label label-important">浮点数</span></label>
				<input type="number" name="scenicspots_longitude" value="<{$_POST.scenicspots_longitude}>" class="input-xlarge" required="true">
				<label>纬度 <span class="label label-important">浮点数</span></label>
				<input type="number" name="scenicspots_latitude" value="<{$_POST.scenicspots_latitude}>" class="input-xlarge" required="true">
				<label>语音 <span class="label label-important">支持mp3,wmv格式,文件大小不超过2M</span></label>
				<input type="file" name="scenicspots_voiceurl" class="input-xlarge"  />
				<label>封面 <span class="label label-important">支持jpg,bmp,png格式,文件大小不超过1M</span></label>
				<input type="file" name="scenicspots_cover" class="input-xlarge" />
				
				<div class="btn-toolbar">
					<button type="submit" class="btn btn-primary"><strong>提交</strong></button>
				</div>
			</form>
        </div>
    </div>
</div>
<!-- END 以下内容不需更改，请保证该TPL页内的标签匹配即可 -->
<{include file="footer.tpl" }>