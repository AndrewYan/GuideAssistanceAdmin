<{include file ="header.tpl"}>
<{include file ="navibar.tpl"}>
<{include file ="sidebar.tpl"}>

<!--- START 以上内容不需更改，保证该TPL页内的标签匹配即可 --->

<{$osadmin_action_alert}>
<{$osadmin_quick_note}>
<!--

//-->
</script>
<div class="btn-toolbar" style="margin-bottom:2px;">
    <a href="user_add.php" class="btn btn-primary"><i class="icon-plus"></i> 活动</a>
</div>
    <div class="block">
        <a href="#page-stats" class="block-heading" data-toggle="collapse">活动列表</a>
        <div id="page-stats" class="block-body collapse in">
               <table class="table table-striped">
              <thead>
                <tr>
					<th style="width:80px">活动编码</th>
					<th style="width:80px">活动标题</th>
					<th style="width:100px">活动背景</th>
					<th style="width:100px">活动地点</th>
					<th style="width:80px">活动开始时间</th>
					<th style="width:80px">活动结束时间</th>
					<th style="width:80px">预约电话</th>
					<th style="width:80px">活动内容</th>
					<th style="width:80px">启用状态</th>
					<th style="width:80px">活动封面</th>
					<th style="width:80px">发布时间</th>
					<th style="width:80px">排序字段</th>
					<th style="width:80px">操作</th>
                </tr>
              </thead>
              <tbody>							  
                <{foreach name=activities from=$activities item=activity}>				 
					<tr>
					<td><{$activity.ActivityCode}></td>
					<td><{$activity.ActivityTitle}></td>
					<td><{$activity.TheBackground}></td>
					<td><{$activity.ActivityLocation}></td>
					<td><{$activity.ActivityTime}></td>
					<td><{$activity.ActivityEndTime}></td>
					<td><{$activity.PhoneNum}></td>
					<td><{$activity.ActivityContent}></td>
					<td><{if $activity.IsActivity}>
						启用
					<{else}>
						停用
					<{/if}>
					</td>
					<td><{$activity.ActivityCover}></td>
					<td><{$activity.PublishTime}></td>
					<td><{$activity.OrderNum}></td>
					<td>
					<a href="user_modify.php?user_id=<{$user_info.user_id}>" title= "修改" ><i class="icon-pencil"></i></a>
					&nbsp;
					
					<{if $activity.IsActivity == 1}>
					<a data-toggle="modal" href="#myModal"  title= "封停账号" ><i class="icon-pause" href="activities.php?page_no=<{$page_no}>&method=pause&ActivityCode=<{$activity.ActivityCode}>"></i></a>
					<{/if }>
					<{if $activity.IsActivity == 0}>
					<a data-toggle="modal" href="#myModal" title= "解封账号" ><i class="icon-play" href="activities.php?page_no=<{$page_no}>&method=play&ActivityCode=<{$activity.ActivityCode}>"></i></a>
					<{/if }>
					</td>
					</tr>
				<{/foreach}>
              </tbody>
            </table> 
				<!--- START 分页模板 --->
				
               <{$page_html}>
					
			   <!--- END --->
        </div>
    </div>

<!---操作的确认层，相当于javascript:confirm函数--->
<{$osadmin_action_confirm}>

<!--- END 以下内容不需更改，请保证该TPL页内的标签匹配即可 --->
<{include file="footer.tpl" }>