

<{include file ="header.tpl"}>
<{include file ="navibar.tpl"}>
<{include file ="sidebar.tpl"}>
<!--- START 以上内容不需更改，保证该TPL页内的标签匹配即可 --->

<{$osadmin_action_alert}>
<{$osadmin_quick_note}>
<script charset="utf-8" src="<{$smarty.const.ADMIN_URL}>/include/kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="<{$smarty.const.ADMIN_URL}>/include/kindeditor/lang/en.js"></script>
<link rel="stylesheet" href="<{$smarty.const.ADMIN_URL}>/include/kindeditor/themes/simple/simple.css">
<script type="text/javascript">

alert("aaaa");
$(function(){
KindEditor.ready(function(K) {
    window.editor = K.create("#editor_id",{
      
      themeType:"default",
      langType : "zh_CN",
      items: ["source","preview","code","|","cut","copy","paste","|","justifyleft","justifycenter","justifyright",
"justifyfull","|","insertorderedlist","insertunorderedlist","indent","outdent","|","subscript",
"superscript","clearhtml","fullscreen","/",
"formatblock","fontname","fontsize","|","forecolor","hilitecolor","bold",
"italic","underline","lineheight","removeformat","|","image","multiimage","table","emoticons","|",
"anchor","link","unlink"],
      minWidth: 400,
    });
})
});


</script>

<div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">请填写功能资料</a></li>
    </ul>	
	
	<div id="myTabContent" class="tab-content">
		  <div class="tab-pane active in" id="home">

           <form id="tab" name="form_content" method="post" action="" enctype="multipart/form-data">
				<label>攻略标题</label>
				<input type="text" name="StrategyTitle" value="<{$_POST.StrategyTitle}>" class="input-xlarge" required="true" autofocus="true">
				<label>封面图片<span class="label label-important">*只允许上传jpg|png|bmp|pjpeg|gif格式的图片</span></label>
				<input type="file" class="input-xlarge" name="file">
				<div class="btn-toolbar">
					<button type="submit" class="btn btn-primary"><strong>上传</strong></button>
				</div>
				<label>模块排序数字(数字越小越靠前)</label>
				<input type="text" name="OrderNum" value="<{$_POST.OrderNum}>" class="input-xlarge" >			
				<label>内容</label>
				<!-- <{html_kindeditor name="kindcontent" width="700" height="250" theame="simple" lang="en" items="simple"}>  -->
				<textarea id="editor_id" name="kindcontent" width="700" height="250"></textarea>
                </textarea>
				<div class="btn-toolbar">
					<button type="submit" class="btn btn-primary" ><strong>提交</strong></button>
				</div>
			</form>
        </div>
    </div>
</div>


<!-- END 以下内容不需更改，请保证该TPL页内的标签匹配即可 -->
<{include file="footer.tpl" }>