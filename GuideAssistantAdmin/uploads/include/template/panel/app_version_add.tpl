<{include file ="header.tpl"}>
<{include file ="navibar.tpl"}>
<{include file ="sidebar.tpl"}>
<!-- START 以上内容不需更改，保证该TPL页内的标签匹配即可 -->

<{$osadmin_action_alert}>
<{$osadmin_quick_note}>
    
<div class="well">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">请填写版本资料</a></li>
    </ul>	
	
	<div id="myTabContent" class="tab-content">
		  <div class="tab-pane active in" id="home">

           <form id="tab" method="post" action="">
				<label>版本号</label>
				<input type="text" name="VersionCode" value="<{$_POST.VersionCode}>" class="input-xlarge" required="true" autofocus="true" >
				<label>版本名称</label>
				<input type="text" name="VersionName" value="<{$_POST.VersionName}>" class="input-xlarge" required="true" autofocus="true" >
				<label>版本标题</label>
				<input type="text" name="VersionTitle" value="<{$_POST.VersionTitle}>" class="input-xlarge" required="true" autofocus="true" >
				<label>版本简介</label>
				<textarea name="VersionBrief" rows="5" class="input-xlarge"><{$_POST.VersionBrief}></textarea>
				<label>下载链接</label>
				<input type="url" name="DownloadUrl" value="<{$_POST.DownloadUrl}>" class="input-xlarge" required="true" autofocus="true" >
				<div class="btn-toolbar">
					<button type="submit" class="btn btn-primary"><strong>提交</strong></button>
				</div>
			</form>
        </div>
    </div>
</div>
<!-- END 以下内容不需更改，请保证该TPL页内的标签匹配即可 -->
<{include file="footer.tpl" }>