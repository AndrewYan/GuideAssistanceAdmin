<{include file ="header.tpl"}>
<{include file ="navibar.tpl"}>
<{include file ="sidebar.tpl"}>
<!--- START 以上内容不需更改，保证该TPL页内的标签匹配即可 --->

<{$osadmin_action_alert}>
<{$osadmin_quick_note}>
<div class="btn-toolbar" style="margin-bottom:2px;">
    <a href="touring_guide_add.php" class="btn btn-primary"><i class="icon-plus"></i> 游赏攻略</a>
</div>
    <div class="block">
        <a href="#page-stats" class="block-heading" data-toggle="collapse">客户端信息</a>
        <div id="page-stats" class="block-body collapse in">
               <table class="table table-striped">
              <thead>
                <tr>
					<th style="width:50px">系统编码</th>
					<th style="width:50px">攻略标题</th>
					<th style="width:50px">攻略封面</th>
					<th style="width:80px">攻略内容</th>
					<th style="width:80px">点赞数量</th>
					<th style="width:80px">排序</th>
					<th style="width:80px">是否启用</th>
					<th style="width:100px">操作</th>
                </tr>
              </thead>
              <tbody>							  
                <{foreach name=TouringGuides from=$TouringGuides item=TouringGuide}>
					<tr>
					<td><{$TouringGuide.StrategyCode}></td>
					<td><{$TouringGuide.StrategyTitle}></td>
					<td><{$TouringGuide.StrategyCover}></td>
					<td><{$TouringGuide.StrategyContent}></td>
					<td><{$TouringGuide.GoodNum}></td>
					<td><{$TouringGuide.OrderNum}></td>
					<td><{if $TouringGuide.IsActivity}>
						启用
					<{else}>
						停用
					<{/if}>
					</td>
					<td>
					<a href="user_modify.php?StrategyCode=<{$TouringGuide.StrategyCode}>" title= "修改" ><i class="icon-pencil"></i></a>
					&nbsp;
					<{if $TouringGuide.IsActivity == 1}>
					<a data-toggle="modal" href="#myModal"  title= "封停账号" ><i class="icon-pause" href="touring_guides.php?page_no=<{$page_no}>&method=pause&StrategyCode=<{$TouringGuide.StrategyCode}>"></i></a>
					<{/if }>
					<{if $TouringGuide.IsActivity == 0}>
					<a data-toggle="modal" href="#myModal" title= "解封账号" ><i class="icon-play" href="touring_guides.php?page_no=<{$page_no}>&method=play&StrategyCode=<{$TouringGuide.StrategyCode}>"></i></a>
					<{/if }>
					&nbsp;
					</td>
					
					</tr>
				<{/foreach}>
              </tbody>
            </table>
				<!--- START 分页模板 --->
               <{$page_html}>
			   <!--- END --->
        </div>
    </div>
    
<!---操作的确认层，相当于javascript:confirm函数--->
<{$osadmin_action_confirm}>
	
<!--- END 以下内容不需更改，请保证该TPL页内的标签匹配即可 --->
<{include file="footer.tpl" }>