<{include file ="header.tpl"}>
<{include file ="navibar.tpl"}>
<{include file ="sidebar.tpl"}>
<!--- START 以上内容不需更改，保证该TPL页内的标签匹配即可 --->

<{$osadmin_action_alert}>
<{$osadmin_quick_note}>

<div style="border:0px;padding-bottom:5px;height:auto">
	<form action="" method="GET" style="margin-bottom:0px">
	<div style="float:left;margin-right:5px">
		<label> 选择起始时间 </label>
		<input type="text" id="start_date" name="start_date" value="<{$_GET.start_date}>" placeholder="起始时间" >
	</div>
	<div style="float:left;margin-right:5px">
		<label>选择结束时间</label>	
		<input type="text" id="end_date" name="end_date" value="<{$_GET.end_date}>" placeholder="结束时间" > 
	</div>
	<div class="btn-toolbar" style="padding-top:25px;padding-bottom:0px;margin-bottom:0px">
		<button type="submit" class="btn btn-primary"><strong>检索</strong></button>
	</div>
	<div style="clear:both;"></div>
	</form>
</div>
    <div class="block">
        <a href="#page-stats" class="block-heading" data-toggle="collapse">客户端信息</a>
        <div id="page-stats" class="block-body collapse in">
               <table class="table table-striped">
              <thead>
                <tr>
					<th style="width:30px">#</th>
					<th style="width:50px">手机名称</th>
					<th style="width:50px">手机号码</th>
					<th style="width:50px">本地IP</th>
					<th style="width:80px">IMEI</th>
					<th style="width:80px">IMSI</th>
					<th style="width:80px">本地MAC</th>
					<th style="width:80px">PSN</th>
					<th style="width:80px">登录时间</th>
                </tr>
              </thead>
              <tbody>							  
                <{foreach name=userPhoneInfo from=$UserPhoneInfo item=info}>
					<tr>
					<td><{$info.ID}></td>
					<td><{$info.PhoneName}></td>
					<td><{$info.PhoneNumber}></td>
					<td><{$info.LocalIP}></td>
					<td><{$info.IMIE}></td>
					<td><{$info.IMSI}></td>
					<td><{$info.LocalMAC}></td>
					<td><{$info.PSN}></td>
					<td><{$info.LoginTime}></td>
					
					</tr>
				<{/foreach}>
              </tbody>
            </table>
				<!--- START 分页模板 --->
               <{$page_html}>
			   <!--- END --->
        </div>
    </div>
    
<script>
$(function() {
	var date=$( "#start_date" );
	date.datepicker({ dateFormat: "yy-mm-dd" });
	date.datepicker( "option", "firstDay", 1 );
});
$(function() {
	var date=$( "#end_date" );
	date.datepicker({ dateFormat: "yy-mm-dd" });
	date.datepicker( "option", "firstDay", 1 );
});
</script>
	
<!--- END 以下内容不需更改，请保证该TPL页内的标签匹配即可 --->
<{include file="footer.tpl" }>