<{include file ="header.tpl"}>
<{include file ="navibar.tpl"}>
<{include file ="sidebar.tpl"}>
<!-- TPLSTART 以上内容不需更改，保证该TPL页内的标签匹配即可 -->

<{$osadmin_action_alert}>
<{$osadmin_quick_note}>

<div class="btn-toolbar">
 	<a href="scenicspots_add.php"  class="btn btn-primary"><i class="icon-plus"></i> 景点</a>
</div>
<div class="block">
	<a href="#page-stats" class="block-heading" data-toggle="collapse">景点列表</a>
	<div id="page-stats" class="block-body collapse in">
		<table class="table table-striped">
			<thead>
			<tr>
				<th>编码</th>
				<th>名称</th>
				<th>所属镇区编码</th>
				<th>票价</th>
				<th>介绍</th>
				<th>经度</th>
				<th>纬度</th>
				<th>语音链接</th>
				<th>封面链接</th>
				<th>操作</th>
			</tr>
			</thead>
			<tbody>							  
			<{foreach name=data from=$scenicspots item=data}>
				<tr>
				<td><{$data.ScenicSpotsCode}></td>
				<td><{$data.ScenicSpotsName}></td>
				<td><{$data.TownshipCode}></td>
				<td><{$data.TicketPrice}></td>
				<td><{$data.ScenicSpotsSummary}></td>
				<td><{$data.ScenicSpotsLongitude}></td>
				<td><{$data.ScenicSpotsLatitude}></td>
				<td><{$data.VoiceURL}></td>
				<td><{$data.ScenicSpotsCover}></td>
				<td>
					<a href="scenicspots_modify.php?scenicspots_code=<{$data.ScenicSpotsCode}>" title= "修改" ><i class="icon-pencil"></i></a>
					&nbsp;
					
					<{if $data.IsActivity == 1}>
					<a data-toggle="modal" href="#myModal"  title= "停用" ><i class="icon-pause" href="scenicspots.php?page_no=<{$page_no}>&method=pause&scenicspots_code=<{$data.ScenicSpotsCode}>"></i></a>
					<{/if }>
					<{if $data.IsActivity == 0}>
					<a data-toggle="modal" href="#myModal" title= "开启" ><i class="icon-play" href="scenicspots.php?page_no=<{$page_no}>&method=play&scenicspots_code=<{$data.ScenicSpotsCode}>"></i></a>
					<{/if }>
				</td>
			   </tr>
			<{/foreach}>
		  </tbody>
		</table>
			<!--- START 分页模板 --->
				
               <{$page_html}>
					
			 <!--- END --->
	</div>
</div>

<!---操作的确认层，相当于javascript:confirm函数--->
<{$osadmin_action_confirm}>
	
<!-- TPLEND 以下内容不需更改，请保证该TPL页内的标签匹配即可 -->
<{include file="footer.tpl" }>