<{include file ="header.tpl"}>
<{include file ="navibar.tpl"}>
<{include file ="sidebar.tpl"}>
<!--- START 以上内容不需更改，保证该TPL页内的标签匹配即可 --->

<{$osadmin_action_alert}>
<{$osadmin_quick_note}>

<div class="btn-toolbar">
	<a href="version_add.php" class="btn btn-primary"><i class="icon-plus"></i> 版本</a>
</div>
<div class="block">
	<a href="#page-stats" class="block-heading" data-toggle="collapse">版本信息列表</a>
	<div id="page-stats" class="block-body collapse in">
		<table class="table table-striped">
			<thead>
			<tr>
				<th>#</th>
				<th>版本号</th>
				<th>版本名称</th>
				<th>版本标题</th>
				<th>版本简介</th>
				<th>下载链接</th>
				<th>是否为最新版</th>
				<th width="80px">操作</th>
			</tr>
			</thead>
			<tbody>							  
			<{foreach name=version from=$versions item=version}>
				 
				<tr>
				 <td><{$version.ID}></td>
				<td><{$version.VersionCode}></td>
				<td><{$version.VersionName}></td>
				<td><{$version.VersionTitle}></td>
				<td><{$version.VersionBrief}></td>
				<td><{$version.DownloadUrl}></td>
				<td>
					<{if $version.IsLastVersion}>
						新版
					<{else}>
						旧版
					<{/if}>
				</td>
				<td>
				<a href="version_modify.php?ID=<{$version.ID}>" title= "修改" ><i class="icon-pencil"></i></a>
				</td>
				</tr>
			<{/foreach}>
		  </tbody>
		</table>  
	</div>
</div>

<!---操作的确认层，相当于javascript:confirm函数--->
<{$osadmin_action_confirm}>
	
<!-- TPLEND 以下内容不需更改，请保证该TPL页内的标签匹配即可 -->
<{include file="footer.tpl" }>